#!/usr/bin/env python3

import subprocess

print("I will set up git aliases for you now.")

# http://durdn.com/blog/2012/11/22/must-have-git-aliases-advanced-examples/
print("git ls")
subprocess.call("git config --global alias.ls \"log --pretty=format:'%C(yellow)%h%C(red)%d\ %C(reset)%s%C(blue)\ [%cn]' --decorate\"", shell=True)

print("git unstage filename - to untage a file")
subprocess.call("git config --global alias.unstage \"reset HEAD --\"", shell=True)

print("git last - for last commit")
subprocess.call("git config --global alias.last \"log -1 HEAD\"", shell=True)

print("git ls - for a short list of commits")
subprocess.call("git config --global alias.ls \"log --pretty=format:'%C(yellow)%h%Cred%d\\ %Creset%s%Cblue\\ [%cn]' --decorate\"", shell=True)

print("git ll - for a list of commits with changed files")
subprocess.call("git config --global alias.ll \"log --pretty=format:'%C(yellow)%h%Cred%d\\ %Creset%s%Cblue\\ [%cn]' --decorate --numstat\"", shell=True)

print("git filelog [filename] - for a log diff")
subprocess.call("git config --global alias.filelog = \"log -u\"", shell=True)

# https://stackoverflow.com/questions/1838873/visualizing-branch-topology-in-git/34467298#34467298
print("git tree - for a complete commit graph")
subprocess.call("git config --global alias.tree-branch \"log --graph --abbrev-commit --decorate --format=format:'%C(bold blue)%h%C(reset) - %C(bold green)(%ar)%C(reset) %C(white)%s%C(reset) %C(dim white)- %an%C(reset)%C(auto)%d%C(reset)'\"", shell=True)

print("git tree-branch master - for a complete commit graph of master")
subprocess.call("git config --global alias.tree !\"git tree-branch --all\"", shell=True)